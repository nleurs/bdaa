<?php
/**
 * Created by PhpStorm.
 * User: nleurs
 * Date: 29/04/19
 * Time: 14:57
 */

class Mysql
{

    CONST DEFAULT_USER = "root";
    CONST DEFAULT_PASSSWORD = "root";
    CONST DEFAULT_HOST = "localhost";
    CONST DEFAULT_DBNAME = "test";

    private $bdd;

    private static $mysqlInstance;

    public function __construct()
    {
        try {
            $this->bdd = new PDO("mysql:host=" . self::DEFAULT_HOST . ";dbname=" . self::DEFAULT_DBNAME, self::DEFAULT_USER, self::DEFAULT_PASSSWORD);
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function getInstance()
    {
        if (is_null(self::$mysqlInstance)) {
            self::$mysqlInstance = new Mysql();
        }
        return self::$mysqlInstance;
    }

    public function getConnection()
    {
        return $this->bdd;
    }

    public function querry($sql){
        return $this->getConnection()->query($sql);
    }
}