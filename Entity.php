<?php

include_once('EntityInterface.php');
require_once('Mysql.php');

abstract class Entity implements EntityInterface
{

    public function __construct()
    {
        try {
            $reflectionClass = new ReflectionClass(get_class($this));
        } catch (ReflectionException $e) {
        }
        $this->properties = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
        $this->tableName = $reflectionClass->getName();
        $this->mysql = new Mysql();
    }

    public function save()
    {

        //var_dump($className);
        //var_dump($properties);

        //INSERT INTO  table (properties, ...) VALUES (values, ...)

        $props = array();

        foreach ($this->properties as $property) {
            if ($property->getName() !== "id") {
                $props[] = "" . $property->getName() . " = '" . $property->getValue($this) . "'";
            }
        }
        //var_dump($props);

        $sqlLoad = "SELECT * FROM " . $this->tableName . " WHERE id =" . $this->properties[0]->getValue($this);
        $result = Mysql::getInstance()->getConnection()->query($sqlLoad)->fetch(PDO::FETCH_ASSOC);
        if ($result == false) {
            $sqlInsert = "INSERT INTO " . "$this->tableName " . "SET " . implode(" , ", $props);
            //var_dump($sqlInsert);
            $this->mysql->getInstance()->querry($sqlInsert);
        } else {
            $sqlUpdate = "UPDATE " . "$this->tableName " . "SET " . implode(" , ", $props) . " WHERE id = " . $this->properties[0]->getValue($this);
            //var_dump($sqlUpdate);
            $this->mysql->getInstance()->querry($sqlUpdate);
        }
    }

    public function load($id)
    {
        $sqlLoad = "SELECT * FROM " . $this->tableName . " WHERE id =" . $id;
        //var_dump($sqlLoad);
        $result = Mysql::getInstance()->getConnection()->query($sqlLoad)->fetch(PDO::FETCH_ASSOC);
        if ($result == false) {
            // echo "requéte incorect";
        } else {
            foreach ($this->properties as $property) {
                $propertyName = $property->getName();
                $property->setValue($this, $result[$propertyName]);
            }
        }
    }

    public static function find($col, $value): array
    {
        $list = array();
        $table = get_called_class();
        $sqlFind = "SELECT * FROM " . $table . " WHERE " . $col . " ='" . $value . "'";
        //var_dump($sqlFind);
        $result = Mysql::getInstance()->getConnection()->query($sqlFind)->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($result);
        foreach ($result as $lign) {
            //var_dump($lign);
            $post = new Post();
            $listAttribut = array();
            $classe = new ReflectionObject($post);
            foreach ($classe->getProperties() as $attribut) {
                if ($attribut->isDefault()) {
                    $listAttribut[] = $attribut->getName();
                }

            }

            // var_dump($listAttribut);
            $i = 0;
            foreach ($lign as $value) {
                $property = $listAttribut[$i];
                $post->$property = $value;
                //var_dump($property);
                $i++;
            }
            $list[] = $post;
        }
        return $list;
    }

    public function create()
    {
        $json = file_get_contents("config.json");
        $decode = json_decode($json);
        if (self::check($json) == true) {
            $table = $decode->{"name"};

            $sqlTest = "SELECT * from " . $table;
            $result = Mysql::getInstance()->getConnection()->query($sqlTest);
            if ($result != false) {
                $this->alter();
            }
            //var_dump($decode->{"fields"});

            /*
            $str = "";
            foreach ($decode->{"fields"} as $fieldName => $champ) {
                $str = $str . $fieldName . " ";
                $str = $str . $champ->type;
                $str = $str . " " . $champ->property;
                $str = $str . ",";
            }
            $str = substr($str, 0, -2);
            $sqlCreate = "CREATE TABLE " . $table . "(" . $str . ")";
            */

            $list = array();
            foreach ($decode->{"fields"} as $fieldName => $champ) {
                $list[] = $fieldName . " " . $champ->type . " " . $champ->property;
            }
            $sqlCreate = "CREATE TABLE " . $table . "(" . implode($list, ',') . ")";
            //var_dump($sqlCreate);
            $this->mysql->getInstance()->querry($sqlCreate);
        } else {
            echo "JSON incorect";
        }

    }

    public function alter()
    {
        $json = file_get_contents("config.json");
        $decode = json_decode($json);
        if (self::check($json) == true) {
            $table = $decode->{"name"};
        }
        $list = array();
        foreach ($decode->{"fields"} as $fieldName => $champ) {
            //var_dump($fieldName);
            $sqlTestCol = "SELECT " . $fieldName . " FROM " . $table;
            //var_dump($sqlTestCol);
            $result = Mysql::getInstance()->getConnection()->query($sqlTestCol);
            //var_dump($result);
            if ($result == false) {
                $sqlAdd = "ALTER TABLE " . $table . " ADD " . $fieldName . " " . $champ->type . " " . $champ->property;
                $this->mysql->getInstance()->querry($sqlAdd);
                $list[] = $fieldName;
            } else {
                $list[] = $fieldName;
            }

        }
        $sqlTestCol = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where table_name= '" . "$table" . "'";
        $result = Mysql::getInstance()->getConnection()->query($sqlTestCol);
        $list2 = array();
        foreach ($result as $champ) {
            $list2[] = $champ[0];
        }
        $diff = array();
        $diff[] = array_diff($list2, $list);
        foreach ($diff as $champ) {
            foreach ($champ as $value) {
                $sqlRemove = "ALTER TABLE " . $table . " DROP COLUMN " . $value;
                $this->mysql->getInstance()->querry($sqlRemove);
            }
        }
    }

    public
    function check($json): bool // check vérifie si le json est valide
    {
        $decode = json_decode($json);
        $pass = false;
        //test si fields éxiste
        if ($decode->{"fields"}) {
            //pour tous les champs
            foreach ($decode->{"fields"} as $fieldName => $champ) {
                //test si type éxiste
                if ($champ->{"type"}) {
                } else {
                    $pass = true;
                }
            }
        } else {
            $pass = true;
        }

        if ($pass == true) {
            //JSON incorect
            return false;
        } else {
            //JSON correct
            return true;
        }
    }

    public
    function drop($table)
    {
        $sqlDrop = "DROP TABLE " . $table;
        $this->mysql->getInstance()->querry($sqlDrop);
    }


}