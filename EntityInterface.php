<?php

interface EntityInterface
{
    public function save();//sauvegarde une entity en bdd

    public function load($id);//retour l'entity avec l'id $id depuis la bdd

    public static function find($col, $value): array;//retourne une liste d'entity qui corresponde à une clause where donner en paramètre

    public function create();//Créer la table du fichier config

    public function check($json): bool;//vérifie la validité du fichier config

    public function drop($table);//drop la table

    public function alter();//modifier la table du fichier config

}

